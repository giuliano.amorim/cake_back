Versão
1.0.0

Pré Requisitos
MERN
FrameWorks

Link para o projeto no Trello:
https://trello.com/b/Sgatq2RN/creators-cake

Tecnologias utilizadas:
NodeJs
React
MongoDB
Express
Bcryptjs
Cors
Jwt
Bootstrap
Reactstrap
Styled-components
Font 

Deploy:
https://cake-back.herokuapp.com/

GitLab:
BackEnd: https://gitlab.com/giuliano.amorim/cake_back.git
FrontEnd: https://gitlab.com/giuliano.amorim/criador_bolos_front.git

Prints das telas:

Trabalhos futuros: